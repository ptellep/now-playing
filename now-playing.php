<?php
/**
 * Plugin Name: Now Playing
 * Plugin URI: http://www.petertellep.com/plugins/now-playing
 * Description: A simple plugin to display Now Playing data from a JSON file
 * Version: 1.2.1
 * Author: Peter Tellep
 * Author URI: http://www.petertellep.com
 */
 
add_shortcode('now-playing', 'display_now_playing');
add_action('wp_enqueue_scripts','register_my_scripts');

function register_my_scripts(){
    wp_enqueue_style( 'style7', plugins_url( 'css/style.css?id=' .microtime() , __FILE__ ) );
    wp_enqueue_script( 'templatesjs', '//ajax.microsoft.com/ajax/jquery.templates/beta1/jquery.tmpl.min.js', array( 'jquery' ));
    wp_enqueue_script( 'js7', plugins_url( 'js/functions.js?id=' .microtime() , __FILE__), array('templatesjs','jquery','wp-util'));
}

function display_now_playing(  $atts = [], $content = null, $tag = '' ){
    
    $atts = array_change_key_case( (array) $atts, CASE_LOWER );

    $now_playing_atts = shortcode_atts(
        array(
            'recently_played' => 1,
        ), $atts, $tag
    );
    
    if($now_playing_atts['recently_played'] == 0){
        $content .= '<div class="now-playing"></div>';        
    } else {
        $content .= '<div class="recently-played"></div>';
    }
?>
    <script type="text/template" id="tmpl-now-playing-row">
      
        <div class="song_row">
            <div class="song_thumb">
                <# if ( data.WXXX_album_art != "" ) { #>
                    <img src="{{ data.WXXX_album_art }}">
                <# } else { #>
                    <img src="<?= plugins_url("img/note.png",__FILE__) ?>">
                <# } #>
            </div>
            <div class="song_detail">
                <div class="song_title">{{ data.TIT2 }}</div><div class="song_artist">{{ data.TPE1 }}</div><div class="song_played">{{ data.played_on }}</div>
            </div>
        </div>
      
    </script>
    <?php 
    $content .= '<script> var nowPlayingURL = "' .  get_option('now_playing_url_option_name') . '"</script>';
    $content .= '<script> var nowPlayingLink = "' .  get_option('now_playing_link_url') . '"</script>';
    return $content;
}

// Admin 

function now_playing_register_settings() {
   add_option( 'now_playing_option_name', 'This is my option value.');
   register_setting( 'now_playing_options_group', 'now_playing_url_option_name', 'now_playing_callback' );
   register_setting( 'now_playing_options_group', 'now_playing_link_url', 'now_playing_callback' );

}
add_action( 'admin_init', 'now_playing_register_settings' );

function now_playing_register_options_page() {
  add_options_page('Now Playing', 'Now Playing', 'manage_options', 'now_playing', 'now_playing_options_page');
}
add_action('admin_menu', 'now_playing_register_options_page');


function now_playing_options_page()
{
?>
  <div>
  <?php screen_icon(); ?>
  <h2>Now Playing</h2>
  <form method="post" action="options.php">
  <?php settings_fields( 'now_playing_options_group' ); ?>
  <h3>Now Playing Settings</h3>
  <p>Currently, this only works with Futuri Streaming.</p>
  <table>
  <tr valign="top">
  <th scope="row"><label for="now_playing_url_option_name">Now Playing URL (JSON):</label></th>
  <td><input type="text" size="65" id="now_playing_url_option_name" name="now_playing_url_option_name" value="<?php echo get_option('now_playing_url_option_name'); ?>" /></td>
  </tr>
  <tr valign="top">
  <th scope="row"><label for="now_playing_link_url">URL for your player:</label></th>
  <td><input type="text" size="65" id="now_playing_link_url" name="now_playing_link_url" value="<?php echo get_option('now_playing_link_url'); ?>" /></td>
  </tr>
  </table>

  <?php  submit_button(); ?>
  </form>
  </div>
<?php
}