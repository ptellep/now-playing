var $ = jQuery;
function convertPlayTime(unix_timestamp){
    var date = new Date(unix_timestamp * 1000);
    console.log('Played On:', date);
    var hours = date.getHours();
    var hour = (hours+24)%24;
    var mid='am';
    if(hours==0){ //At 00 hours we need to show 12 am
        hours=12;
    } else if(hours>12) {
        hours=hours%12;
        mid='pm';
    }
    var minutes = "0" + date.getMinutes();
    var seconds = "0" + date.getSeconds();
    var formattedTime = hours + ':' + minutes.substr(-2) + mid;// + ':' + seconds.substr(-2);

    return formattedTime;    
}

var songs=[];
var template = wp.template( 'now-playing-row' );

function initNowPlaying(url){
    $.getJSON(url, function(data) {
        var idx = 0;
        $.each(data, function(index,song){
            if(song.TXXX_category == "music"  || (song.TIT2 != "" && song.TPE1 != "")){
                idx++;
                songs.push(song);
                song.played_on = convertPlayTime(song.played_on);
                var html = template( song );
                if(idx == 1){
                    $('.now-playing').html( html );
                }
                $('.recently-played').append( html );
            }
        });
        attachOpener();
    });
    setInterval(function() {
        updateNowPlaying(url);
    }, 10000);
}

function updateNowPlaying(url){
    $.getJSON(url, function(data) {
        if((data[0].TXXX_category == "music") || (data[0].TIT2 != "" && data[0].TPE1 != "")){
            if(data[0].TIT2 == songs[0].TIT2 ) {
                //console.log('no change');
            } else {
                //console.log('change');
                data[0].played_on = convertPlayTime(data[0].played_on);
                var html = template( data[0] );
                $('.now-playing').html( html );
                $('.recently-played').prepend( html );
                $('.recently-played.song_row').last().fadeOut();
                songs.unshift(data[0]);
                songs.pop();
                attachOpener();
            }
        }
    });
}

function displayNowPlaying(){
    var template = wp.template( 'now-playing-row' );
    $.each(songs, function(index,song){
        if(song.TXXX_category == "music"){
            song.played_on = convertPlayTime(song.played_on);
            var html = template( song );
             $('.now-playing').html( html );
             $( '#recently_played' ).append( html );
             attachOpener();
        }
    });
}

function attachOpener(){
    $('.song_row').on('click', function(){
        console.log('Clicked');
        var win = window.open(nowPlayingLink, '_blank');
    });
}

jQuery(document).ready(function(){
    initNowPlaying(nowPlayingURL);

})