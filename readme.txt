Now Playing Plugin

!!! ATTENTION !!! - This plugin is offered with no waranties or guarantees.  
Make sure you have a back up of your site.  You use this at your own risk.  By
using this plugin you take on the responsibilty and any and all liability.

Set Up:

Install the plugin by uploading the .zip file, and activate the plugin

Under "Settings" in your WordPress Admin, select "Now Playing"

On the admin page, add the URL for your now playing data.  This plugin currently
works only with Futuri/StreamOn now playing feeds.

Use:

  To show the currently playing song

    Create a page or HTML widget and use the following shortcode:

      [now-playing recently_played=0]

  To show the recently played songs including the current song use:
  
    Create a page or HTML widget and use the following shortcode:

      [now-playing]
      
     